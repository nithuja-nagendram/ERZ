<?php
require_once('index.php');

    $Client1= new ApiClient();
    /*echo "<pre>";
    var_dump($Client1->erzData(null,"organic", 6));
    echo "<pre>";*/
    $zip = null;
	$waste = null;
	$station = null;
	$startdatum = null;
	$enddatum = null;
	$page = 1;
	$totalpages = 1;
	$sort =null;
	$i = null;
	$data = $Client1->erzData($zip, $waste,$page, $station, $startdatum, $enddatum, $sort);
	$dataStations = $Client1->getStations();
	//var_dump($dataStations); die;
	//$datum = date("Ymd",$timestamp);

	if (!empty ($_GET['zip'])) {
		$zip = $_GET['zip'];
	}
	if (!empty ($_GET['waste'])) {
		$waste = $_GET['waste'];
	}
	if (!empty ($_GET['page'])) {
		$page = $_GET['page'];
	}
	if (!empty($_GET['station'])){
		$station = $_GET['station'];
	}
	if (!empty($_GET['startdatum'])){
		$startdatum = $_GET['startdatum'];
	}
	if (!empty($_GET['enddatum'])){
		$enddatum = $_GET['enddatum'];
	}
	if (!empty($_GET['sort'])){
		$sort = $_GET['sort'];
	}

	function paper(){
		if (isset($_GET['waste']) && $_GET['waste'] == 'paper'){ 		
			echo 'checked="checked"';
		}
	}
	function textile(){
		if (isset($_GET['waste']) && $_GET['waste'] == 'textile'){ 		
			echo 'checked="checked"';
		}
	}
	function organic(){
		if (isset($_GET['waste']) && $_GET['waste'] == 'organic'){ 		
			echo 'checked="checked"';
		}
	}
	function special(){
		if (isset($_GET['waste']) && $_GET['waste'] == 'special'){ 		
			echo 'checked="checked"';
		}
	}
	function etram(){
		if (isset($_GET['waste']) && $_GET['waste'] == 'etram'){ 		
			echo 'checked="checked"';
		}
	}
	
 ?>

<!DOCTYPE HTML>
<html>
	<head>
		<title>ErzData</title>
		<meta charset="utf-8">
	    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="main.css">
        <script src="//code.jquery.com/jquery-1.10.2.js"></script>
        <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
		<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
	    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	  	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	 	<link rel="stylesheet" href="/resources/demos/style.css">
        <script language="javascript" type="text/javascript">

				function validateForm() {
			    	var x = document.forms["erzForm"]["zip"].value;
				    if (isNaN(x)) {
				        alert("PLZ muss ein Zahl ( < 1000) sein!");
				        return false;
	    			}
	    		}
	    		$(function() {
				    $("#datepicker1" ).datepicker({ dateFormat: "yy-mm-dd" });
				    $("#datepicker2").datepicker({ dateFormat: "yy-mm-dd" })
				});

				$(document).ready(function() {
				  $(".js-example-basic-single").select2();
				});

        </script>
	</head>
		<body>

			<h1>Welcome to Open ERZ-Calendar</h1>
			<form name="erzForm" action="erz.php" onsubmit="return validateForm()" method="get" >
					<div class="form-group">
						 <label for="zip">PLZ</label>
					    <input type="text" name="zip" class="form-control" value="<?php echo $zip;?>" id="zip">
				  	</div>
				  	<h5>Art</h5>
				  	<div class="radio">
  						<label>
					    <input type="radio"  id="optionsRadios1"  value="paper" name="waste"<?php paper();?> >paper
					  </label>
					</div>
					<div class="radio">
	  					<label>
						    <input type="radio" name="waste" id="optionsRadios2" value="textile" <?php textile();?>> textile
						</label>
					</div>
					<div class="radio">
	  					<label>
						    <input type="radio" name="waste" id="optionsRadios3" value="organic"<?php organic();?> >organic
						 </label>
					</div>
					<div class="radio">
		  				<label>
							  <input type="radio" name="waste" id="optionsRadios4" value="special"<?php special();?> >special
						</label>
					</div>
					<div class="radio">
		  				<label>
							  <input type="radio" name="waste" id="optionsRadios5" value="etram" <?php etram();?>>etram
						</label>
					</div>
					</div>
					</br>
					<h5>Wo</h5>
					<div class="radio">
		  				<label>
								<select class="js-example-basic-single" name="stations">
								<option></option>
									<?php 
									//var_dump($dataStations->result);
									if($_GET['stations'] == $options->name) echo'selected=\"selected\"';
									foreach ($dataStations->result as $options) {
										
										echo "<option value='" . $options->name . "''>". $options->name ." </option>";
									}

									

									?>
								</select>
								
						</label>
					</br>
					</br>
					</div>
					<div class="form-group">
						<label for="start">Anfangs Datum</label>
					    <input type="text" name="startdatum" class="form-control" id="datepicker1" value="<?php echo $startdatum;?>" id="start">
				  	</div>
					<div class="form-group">
						 <label for="end">Schluss Datum</label>
					    <input type="text" name="enddatum" class="form-control" id="datepicker2" value="<?php echo $enddatum;?>" id="end">
				  	</div>
					<div class="form-group">
				      	<button type="submit" class="btn btn-default">suchen</button>
					</div>

					<input type="hidden" name="page" value="1">


			</form>
			<br>
    			<br>
			   <table class="table table-bordered">
			      <thead>
			        <tr>
			        	<th><a href='erz.php?zip=<?php echo $zip;?>&station=<?php echo $station;?>&waste=<?php echo $waste;?>&startdatum=<?php echo $startdatum;?>&enddatum=<?php echo $enddatum;?>&sort=zip:desc&page=<?php echo $page;?>'>
			        	<span class="glyphicon glyphicon-arrow-down" aria-hidden="true"/></a>PLZ
			        	<a href='erz.php?zip=<?php echo $zip;?>&station=<?php echo $station;?>&waste=<?php echo $waste;?>&startdatum=<?php echo $startdatum;?>&enddatum=<?php echo $enddatum;?>&sort=zip:asc&page=<?php echo $page;?>'>
			        	<span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span></a></th>
			        	<th><a href='erz.php?zip=<?php echo $zip;?>&station=<?php echo $station;?>&waste=<?php echo $waste;?>&startdatum=<?php echo $startdatum;?>&enddatum=<?php echo $enddatum;?>&sort=date:desc&page=<?php echo $page;?>'>
			        	<span class="glyphicon glyphicon-arrow-down" aria-hidden="true"/></a>Datum
			        	<a href='erz.php?zip=<?php echo $zip;?>&station=<?php echo $station;?>&waste=<?php echo $waste;?>&startdatum=<?php echo $startdatum;?>&enddatum=<?php echo $enddatum;?>&sort=date:asc&page=<?php echo $page;?>'>
			        	<span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span></a></th>
			        	<th><a href='erz.php?zip=<?php echo $zip;?>&station=<?php echo $station;?>&waste=<?php echo $waste;?>&startdatum=<?php echo $startdatum;?>&enddatum=<?php echo $enddatum;?>&sort=type:desc&page=<?php echo $page;?>'>
			        	<span class="glyphicon glyphicon-arrow-down" aria-hidden="true"/></a>Art
			        	<a href='erz.php?zip=<?php echo $zip;?>&station=<?php echo $station;?>&waste=<?php echo $waste;?>&startdatum=<?php echo $startdatum;?>&enddatum=<?php echo $enddatum;?>&sort=tyoe:asc&page=<?php echo $page;?>'>
			        	<span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span></a></th>
			        	<th><a href='erz.php?zip=<?php echo $zip;?>&station=<?php echo $station;?>&waste=<?php echo $waste;?>&startdatum=<?php echo $startdatum;?>&enddatum=<?php echo $enddatum;?>&sort=station:desc&page=<?php echo $page;?>'>
			        	<span class="glyphicon glyphicon-arrow-down" aria-hidden="true"/></a>Wo
			        	<a href='erz.php?zip=<?php echo $zip;?>&station=<?php echo $station;?>&waste=<?php echo $waste;?>&startdatum=<?php echo $startdatum;?>&enddatum=<?php echo $enddatum;?>&sort=station:asc&page=<?php echo $page;?>'>
			        	<span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span></a></th>
			        </tr>
			      </thead>
      			<tbody>
					<?php

					
					//print_r($_GET);
					if (!empty($_GET)) {
						$data = $Client1->erzData($zip, $waste,$page, $station, $startdatum, $enddatum, $sort);
						 if(!empty($data)) {
							 foreach ($data->result as $row) {
							    echo '<tr>';
							    echo '<td>' . $row->zip . '</td>';
							    $date = new DateTime($row->date);
							    echo '<td>' . $date->format('d.m.Y') . '</td>';		
							    echo '<td><img id="img1"src="img/' . $row->type . '.png">' . $row->type  . '</td>';
							    echo '<td>';
							    if(isset($row->station)) {
							    	echo  $row->station ;	
							    } 
							    echo '</td>';   
							    echo '</tr>';
							 }	

							 if(empty($data->result)) {
							 	echo "Es sind keine Daten vorhanden";
							 }
						} else {
							echo "Keine Daten vorhanden"; 
						}
						  

						$pageSize = 10;
						// totaldaten dada holt eigenschaf meta_daten und greift auf objekten
						$totalCount = $data->_metadata->total_count;
						$totalpages = ceil($totalCount / $pageSize);
						echo "<br />";

					} 

					
					//echo $_GET['page']; <-- ist für die aktuelle page

					?>
      			</tbody>
    			</table>
    				<nav>
					  <ul class="pagination">
					    <?php
						$prev = $page - 1;
						if($prev >= 1) {
						    echo "<li><a href='erz.php?zip=" .$zip . "&station=" .$station . "&waste=" . $waste . "&startdatum=" .$startdatum .  "&enddatum=" .$enddatum . "&sort=" .$sort ."&page=" . $prev .  "'>Prev</a></li>";
							
						}
						// damit nicht 0 angezeigt <wird sondern die erste Seite 1
						$startpage = max($page-1, 1);
						//damit nicht mehr Seiten als totalpages angezeigt wird
						$endpage = min($page+1,$totalpages);
						if ($page == 1) {
							$endpage = min($page+2, $totalpages);

						}
						if($page == $totalpages){
							$startpage = max($page-2, 1);
						}
					    for ($i= $startpage; $i<=$endpage; $i++) {

							if($i == $page) {
								echo "<li><a class='now' href style='background-color: #C0C0C0;'>" . $i . "</a></li>";
							
							} else {
								echo "<li><a href='erz.php?zip=" .$zip . "&station=" .$station . "&waste=" . $waste . "&startdatum=" .$startdatum .  "&enddatum=" .$enddatum ."&sort=" .$sort . "&page=" . $i. "'>" . $i . "</a></li>";
							} 

						}

						$next = $page + 1;
						if($next <= $totalpages) {
					    	echo "<li><a href='erz.php?zip=" .$zip . "&station=" .$station . "&waste=" . $waste . "&startdatum=" .$startdatum .  "&enddatum=" .$enddatum ."&sort=" .$sort . "&page=" . $next . "'>Next</a></li>";
						}

						?>
						  
					  </ul>
					</nav>
		</body>
</html>